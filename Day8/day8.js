const { strictEqual } = require('assert');
const fs = require('fs');

const zero = ['a', 'b', 'c', 'e', 'f', 'g'];
const one = ['c', 'f'];
const two = ['a', 'c', 'd', 'e', 'g'];
const three = ['a', 'c', 'd', 'f', 'g'];
const four = ['b', 'c', 'd', 'f'];
const five = ['a', 'b', 'd', 'f', 'g'];
const six = ['a', 'b', 'd', 'e', 'f', 'g'];
const seven = ['a', 'c', 'f'];
const eight = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
const nine = ['a', 'b', 'c', 'd', 'f', 'g'];

// find 1, 4, 7, 8 count
const part1 = (arr) => {
    arr = arr.map(item => item.split(' | '));

    let countsToSearchFor = [one.length, four.length, seven.length, eight.length];
    let count = 0;
    arr.forEach((pair) => {
        let digits = pair[1].split(' ');
        digits.forEach(digit => {
            let unique = new Set(digit);
            if (countsToSearchFor.includes(unique.size))
                count++
        });
    });

    return count;
}

// part 2 helper functions

// 0 -> only size of 6 that has 3 in common with four, 2 in common with one
// 6 -> only size of 6 that has 1 in common with one
// 9 -> only size 6 that has 4 in common with four, 2 in common with one
const FindZeroSixNine = (candidates, one, four) => {
    let result = {};

    for (let i = 0; i < candidates.length; i++) {
        let oneIntersect = new Set([...candidates[i]].filter(letter => one.has(letter)));
        let fourIntersect = new Set([...candidates[i]].filter(letter => four.has(letter)));

        if (oneIntersect.size === 2) {
            if (fourIntersect.size === 3) {
                result.zero = candidates[i];
            } else { // 9
                result.nine = candidates[i];
            }
        } else { // 6
            result.six = candidates[i];
        }
    }

    if (Object.keys(result).length !== 3)
        throw "Error in FindZeroSixNine logic!";

    return result;
}

// 3 -> only size five with 2 in common with one
// 5 -> only number with five with 5 in common with nine (between two and five)
const FindTwoThreeFive = (candidates, one, nine) => {
    let result = {};

    for (let i = 0; i < candidates.length; i++) {
        let oneIntersect = new Set([...candidates[i]].filter(letter => one.has(letter)));
        let nineIntersect = new Set([...candidates[i]].filter(letter => nine.has(letter)));

        if (oneIntersect.size === 2) { // three
            result.three = candidates[i];
        } else if (nineIntersect.size === 5) { // five
            result.five = candidates[i];
        } else { // two
            result.two = candidates[i];
        }
    }

    if (Object.keys(result).length !== 3)
        throw "Error in FindTwoThreeFive logic!";

    return result;
}

const GetDigits = (mapping, digits) => {
    let result = '';

    for (let i = 0; i < digits.length; i++) {
        let digitStr = digits[i];
        var match = true;

        for (let j = 0; j < mapping.length; j++) {
            if (digits[i].length !== mapping[j].size)
                continue;

            match = true;
            for (let k = 0; k < digitStr.length; k++) {
                if (!mapping[j].has(digitStr[k])) {
                    match = false;
                    break;
                }
            }

            if (match) {
                result += j;
                break;
            }

        }

        if (result.length === 4)
            break;
    }

    return parseInt(result);
}

const part2 = (arr) => {
    arr = arr.map(item => item.split(' | '));
    let mapping = new Array(10);

    let display = [];
    arr.forEach((pair) => {
        let signals = pair[0].split(' ');
        let digits = pair[1].split(' ');

        signals = signals.map(signal => new Set(signal));

        signals.forEach(signal => {
            // map trivial numbers
            switch (signal.size) {
                case one.length:
                    mapping[1] = signal;
                    break;
                case four.length:
                    mapping[4] = signal;
                    break;
                case seven.length:
                    mapping[7] = signal;
                    break;
                case eight.length:
                    mapping[8] = signal;
                    break;
                default:
                    break;
            }

        });

        // solve for 0, 6, 9
        let sizeSix = signals.filter(set => set.size === 6)
        let zeroSixNine = FindZeroSixNine(sizeSix, mapping[1], mapping[4]);
        mapping[0] = zeroSixNine.zero;
        mapping[6] = zeroSixNine.six;
        mapping[9] = zeroSixNine.nine;

        // solve for 2, 3, 5
        let sizeFive = signals.filter(set => set.size === 5);
        let twoThreeFive = FindTwoThreeFive(sizeFive, mapping[1], mapping[9]);
        mapping[2] = twoThreeFive.two;
        mapping[3] = twoThreeFive.three;
        mapping[5] = twoThreeFive.five;

        // get digits
        display.push(GetDigits(mapping, digits));
    });

    return display.reduce((prev, curr) => prev + curr);
}

let arr = fs.readFileSync('./input.txt').toString().trim().split('\n');

console.log(part2(arr));
