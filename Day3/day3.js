
const fs = require('fs');
let arr = fs.readFileSync('./input.txt').toString().trim().split('\n');

const part1 = (arr) => {
    let resultArr = new Array(arr[0].length).fill(0);

    arr.forEach(element => {
        for (let i = 0; i < element.length; i++) {
            switch (element[i]) {
                case '0':
                    resultArr[i]--;
                    break;
                case '1':
                    resultArr[i]++;
                    break;
            }
        }
    });

    let gammaString = '';
    let epsilonString = '';

    resultArr.map((element) => {
        if (element > 0) {
            gammaString += '1'
            epsilonString += '0'
        }
        else {
            gammaString += '0';
            epsilonString += '1';
        }
    });

    let gamma = parseInt(gammaString, 2);
    let epsilon = parseInt(epsilonString, 2);

    return gamma * epsilon;
}

const part2 = (arr) => {
    let O2Sensor = [...arr];
    let CO2Scrubber = [...arr];

    for (let i = 0; i < arr[0].length; i++) {
        // break if already reduced both to 1 value
        if (O2Sensor.length === 1 && CO2Scrubber.length === 1)
            break;

        let O2MostCommon = 0;
        O2Sensor.forEach((item) => {
            if (item[i] == '1')
                O2MostCommon++;
            else
                O2MostCommon--;
        });

        let CO2MostCommon = 0;
        CO2Scrubber.forEach((item) => {
            if (item[i] == '1')
                CO2MostCommon++;
            else
                CO2MostCommon--;
        });

        if (O2Sensor.length > 1) {
            if (O2MostCommon >= 0) {
                O2Sensor = O2Sensor.filter(item => item[i] == '1');
            }
            else {
                O2Sensor = O2Sensor.filter(item => item[i] == '0');
            }
        }

        if (CO2Scrubber.length > 1) {
            if (CO2MostCommon >= 0) {
                CO2Scrubber = CO2Scrubber.filter(item => item[i] == '0');
            }
            else {
                CO2Scrubber = CO2Scrubber.filter(item => item[i] == '1');
            }
        }
    }

    // only 1 number left in O2Sensor and CO2Scrubber -> calculate and return life support rating
    let O2SensorRating = parseInt(O2Sensor[0], 2);
    let CO2ScrubberRating = parseInt(CO2Scrubber[0], 2);

    return O2SensorRating * CO2ScrubberRating;
}

console.log(part1(arr));
console.log(part2(arr));