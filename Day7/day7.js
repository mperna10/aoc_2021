// part1
const MinFuel = (arr) => {
    arr = arr.map(str => parseInt(str));
    arr.sort((num1, num2) => { return num1 - num2 });
    let min = arr[0];
    let max = arr[arr.length - 1];

    let minFuel = Infinity;
    for (let i = min; i <= max; i++) {
        let fuelConsumption = arr.reduce((prev, curr) => prev + Math.abs(curr - i), 0);
        if (fuelConsumption < minFuel)
            minFuel = fuelConsumption;
    }

    return minFuel;
}

// part 2
const MinFuelPart2 = (arr) => {
    arr = arr.map(str => parseInt(str));
    arr.sort((num1, num2) => { return num1 - num2 });
    let min = arr[0];
    let max = arr[arr.length - 1];

    // sum of first n numbers 1...n = n(n + 1) / 2
    const SumOfFirstN = (n) => {
        return (n * (n + 1)) / 2;
    }

    let minFuel = Infinity;
    for (let i = min; i <= max; i++) {
        let fuelConsumption = arr.reduce((prev, curr) => prev + SumOfFirstN(Math.abs(curr - i)), 0);
        if (fuelConsumption < minFuel)
            minFuel = fuelConsumption;
    }

    return minFuel;
}

const fs = require('fs');
let arr = fs.readFileSync('./input.txt').toString().trim().split(',');

console.log(MinFuel(arr));
console.log(MinFuelPart2(arr));