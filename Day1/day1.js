const fs = require('fs');

let arr = fs.readFileSync('./input.txt').toString().split('\n')

const part1 = (arr) => {
    let count = 0;
    for (let i = 1; i < arr.length; i++)
        if (parseInt(arr[i]) > parseInt(arr[i - 1]))
            count++;

    return count;
}

const part2 = (arr) => {
    arr = arr.map((item) => parseInt(item));

    let count = 0;

    for (let i = 1; i < arr.length - 2; i++) {
        let sum1 = arr.slice(i - 1, i + 2).reduce((prev, curr) => prev + curr);
        let sum2 = arr.slice(i, i + 3).reduce((prev, curr) => prev + curr);

        if (sum2 > sum1)
            count++;
    }

    return count;
}


console.log(part2(arr));