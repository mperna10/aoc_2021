const fs = require('fs');
let arr = fs.readFileSync('./input.txt').toString().trim().split('\n')

function SoveParts1And2(arr, part2) {
    // 1000 x 1000 matrix of 0's
    let mat = new Array(1000).fill(null).map(() => new Array(1000).fill(0));

    for (let i = 0; i < arr.length; i++) {
        let coordinates = arr[i].split(' -> ');

        let p1 = coordinates[0].split(',').map(str => parseInt(str));
        let p2 = coordinates[1].split(',').map(str => parseInt(str));

        if (p1[0] == p2[0]) { // vertical line
            let length = Math.abs(p2[1] - p1[1]);
            let x = p1[0];
            let start_y = Math.min(p1[1], p2[1]);
            for (let y = start_y; y <= start_y + length; y++) {
                mat[x][y]++;
            }
        } else if (p1[1] == p2[1]) { // horizontal line
            let length = Math.abs(p2[0] - p1[0]);
            let y = p1[1];
            let start_x = Math.min(p1[0], p2[0]);
            for (let x = start_x; x <= start_x + length; x++) {
                mat[x][y]++;
            }
        } else if (Math.abs(p2[0] - p1[0]) === Math.abs(p2[1] - p1[1]) && part2) { // diagaonal line
            let increasing;
            let start_x;

            if (p2[0] > p1[0]) {
                start_x = p1[0];
                increasing = p2[1] - p1[1] > 0;
            } else {
                start_x = p2[0];
                increasing = p1[1] - p2[1] > 0;
            }

            let length = Math.abs(p2[0] - p1[0]);
            let start_y = increasing ? Math.min(p1[1], p2[1]) : Math.max(p1[1], p2[1]);
            let y_incr = increasing ? 'y++' : 'y--';

            for (let x = start_x, y = start_y; x <= start_x + length; x++) {
                mat[x][y]++;
                eval(y_incr);
            }
        } else { // skip
            continue;
        }
    }

    let count = 0;
    mat.forEach(row => {
        row.forEach(val => {
            if (val > 1)
                count++;
        });
    });

    console.log(count)
}

SoveParts1And2(arr, false);
SoveParts1And2(arr, true);