const fs = require('fs');

const part1 = (arr) => {
    let horizontal = 0;
    let vertical = 0;

    arr.forEach((item) => {
        var values = item.split(' ');
        switch (values[0]) {
            case 'forward':
                horizontal += parseInt(values[1]);
                break;
            case 'up':
                vertical -= parseInt(values[1]);
                break;
            case 'down':
                vertical += parseInt(values[1]);
                break;
            default:
                throw `Unrecognized command - ${values[0]}`
        }
    });

    return horizontal * vertical;
};


const part2 = (arr) => {
    let horizontal = 0;
    let vertical = 0;
    let aim = 0;

    arr.forEach((item) => {
        var values = item.split(' ');
        switch (values[0]) {
            case 'forward':
                horizontal += parseInt(values[1]);
                vertical += (aim * parseInt(values[1]));
                break;
            case 'up':
                aim -= parseInt(values[1]);
                break;
            case 'down':
                aim += parseInt(values[1]);
                break;
            default:
                throw `Unrecognized command - ${values[0]}`
        }
    });

    return horizontal * vertical;
};

let arr = fs.readFileSync('./input.txt').toString().trim().split('\n');

console.log(part2(arr));