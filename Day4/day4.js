const fs = require('fs');

class BingoSquare {
    constructor(num) {
        this.number = num;
        this.marked = false;
    }
}

class BingoCard {
    constructor(inputArr) {
        this.board = [];
        this.winner = false;
        // foreach row of 5 total
        inputArr.forEach((rowString, i) => {
            // replace double space with single, then split on single space to get array
            let rowArray = rowString.trim().replace(/  /g, ' ').split(' ');

            // create new row for this iteration and convert rowArray to ints
            this.board.push(new Array());
            rowArray.map(numString => parseInt(numString));

            // foreach num in row, insert new square in card
            rowArray.forEach((num) => {
                this.board[i].push(new BingoSquare(num));
            }, this);
        }, this);
    }

    IsWinningRow(rowIndex) {
        for (let i = 0; i < this.board[0].length; i++) {
            if (this.board[rowIndex][i].marked === false)
                return false;
        }

        return true;
    }

    IsWinningColumn(colIndex) {
        for (let i = 0; i < this.board.length; i++) {
            if (this.board[i][colIndex].marked === false)
                return false;
        }

        return true;
    }

    MarkNumber(number) {
        for (let i = 0; i < this.board.length; i++) {
            for (let j = 0; j < this.board[0].length; j++) {
                if (this.board[i][j].number == number)
                    this.board[i][j].marked = true;

                if (this.IsWinningRow(i) || this.IsWinningColumn(j)) {
                    return this.board;
                }
            }
        }

        return null;
    }

    CalculateSumOfUnMarked() {
        let sum = 0;
        this.board.forEach(row => {
            row.forEach(square => {
                if (square.marked === false)
                    sum += parseInt(square.number);
            });
        });

        return sum;
    }
}

class BingoGame {
    constructor(dataArray) {
        // remove empty entries
        dataArray = dataArray.filter(el => el != '');

        let numbersString = dataArray[0];
        this.numbers = numbersString.split(',').map(num => parseInt(num));
        this.cards = [];

        for (let i = 1; i < dataArray.length; i += 5)
            this.cards.push(new BingoCard(dataArray.slice(i, i + 5)));
    }

    // find first winner if part1 = true, else find last winner (part 2);
    PlayGame(part1) {
        for (let i = 0; i < this.numbers.length; i++) {
            for (let j = 0; j < this.cards.length; j++) {

                if (this.cards[j].winner === true)
                    continue;

                let result = this.cards[j].MarkNumber(this.numbers[i]);
                if (result) {
                    this.cards[j].winner = true;
                    this.winningNumber = this.numbers[i];
                    this.winningCard = this.cards[j];

                    if (part1)
                        break;
                }
            }

            if (this.winningCard && part1)
                break;
        }
    }

    Part1() {
        this.PlayGame(true);
        let sum = this.winningCard.CalculateSumOfUnMarked();
        return sum * this.winningNumber;
    }

    Part2() {
        this.PlayGame(false);
        let sum = this.winningCard.CalculateSumOfUnMarked();
        return sum * this.winningNumber;
    }

}

(function main() {
    let arr = fs.readFileSync('./input.txt').toString().trim().split('\n')
    let bg = new BingoGame(arr);

    console.log('Part 1', bg.Part1());
    console.log('Part 2', bg.Part2());
})();
