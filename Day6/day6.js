
const FishAfterNDays = (arr, days) => {
    arr = arr.map(el => parseInt(el));

    // array to store # of fish at [n] days until spawn
    let population = new Array(9).fill(0);
    arr.forEach(day => population[day]++);

    for (let i = 0; i < days; i++) {
        // new fish spawned from fish entering current day with 0 days left
        let newfish = population[0];

        // shift the days remaining until spawn one day to the left
        for (let i = 0; i < population.length - 1; i++) {
            population[i] = population[i + 1];
        }

        // add new fish with 8 days and reset the day 0 fish to 6
        population[8] = newfish;
        population[6] += newfish;
    }

    return population.reduce((prev, current) => prev + current);
}


const fs = require('fs');
let arr = fs.readFileSync('./input.txt').toString().split(',');

console.log(FishAfterNDays(arr, 256));