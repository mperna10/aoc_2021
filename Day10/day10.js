const fs = require('fs');

const part1 = (arr) => {
    let errors = [];
    for (charStack of arr) {
        let invalid = false;
        let parsed = []
        for (char of charStack) {
            if (['(', '[', '{', '<'].includes(char)) { // opening char
                parsed.push(char);
            } else { // closing char
                let topStack = parsed.pop();
                switch (char) {
                    case ')':
                        if (topStack !== '(') {
                            errors.push(3);
                            invalid = true;
                        }
                        break;
                    case ']':
                        if (topStack !== '[') {
                            errors.push(57);
                            invalid = true;
                        }
                        break;
                    case '}':
                        if (topStack !== '{') {
                            errors.push(1197);
                            invalid = true;
                        }
                        break;
                    case '>':
                        if (topStack !== '<') {
                            errors.push(25137);
                            invalid = true;
                        }
                        break;
                    default:
                        break;
                }

                if (invalid)
                    break;
            }
        }
    }

    return errors.reduce((prev, curr) => prev += curr);
}

const part2 = (arr) => {
    let scores = [];
    for (charStack of arr) {
        let invalid = false;
        let parsed = []
        for (char of charStack) {
            if (['(', '[', '{', '<'].includes(char)) { // opening char
                parsed.push(char);
            } else { // closing char
                let topStack = parsed.pop();
                switch (char) {
                    case ')':
                        invalid = topStack !== '('
                        break;
                    case ']':
                        invalid = topStack !== '['
                        break;
                    case '}':
                        invalid = topStack !== '{';
                        break;
                    case '>':
                        invalid = topStack !== '<';
                        break;
                    default:
                        break;
                }

                if (invalid)
                    break;
            }
        }

        if (!invalid) { // incomplete
            let total = 0;

            while (parsed.length > 0) {
                total *= 5;
                let current = parsed.pop();
                switch (current) {
                    case '(':
                        total += 1;
                        break;
                    case '[':
                        total += 2;
                        break;
                    case '{':
                        total += 3;
                        break;
                    case '<':
                        total += 4;
                        break;
                }
            }
            scores.push(total);
        }
    }

    scores.sort((a, b) => a - b);
    let middle = Math.floor(scores.length / 2);
    return scores[middle];
}

let input = fs.readFileSync('./input.txt').toString().split('\n').filter(line => line !== '');
let arr = input.map(line => [...line]);

console.log(part2(arr));