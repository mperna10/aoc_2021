const fs = require('fs');

const part1 = (mat) => {
    mat = mat.map(row => row.map(item => {
        return { "value": parseInt(item), "isLowPoint": false };
    }));

    let total = 0;

    for (let i = 0; i < mat.length; i++) {
        for (let j = 0; j < mat[0].length; j++) {
            let currentVal = mat[i][j].value;

            let shiftLeft = i - 1;
            let shiftRight = i + 1;
            let shiftUp = j - 1;
            let shiftDown = j + 1;

            if (shiftLeft >= 0 && mat[shiftLeft][j].value <= currentVal)
                continue;

            if (shiftRight < mat[0].length && mat[shiftRight][j].value <= currentVal)
                continue;

            if (shiftUp >= 0 && mat[i][shiftUp].value <= currentVal)
                continue;

            if (shiftDown < mat.length && mat[i][shiftDown].value <= currentVal)
                continue;

            mat[i][j].isLowPoint = true;
            total += mat[i][j].value + 1;

        }
    }
    return total;
}

const GetBasinSize = (mat, i, j) => {
    // base case: index out of range or square value is 9 -> return 0
    if (i < 0 || i > mat[0].length - 1 || j < 0 || j > mat.length - 1 || mat[i][j].value === 9 || mat[i][j].marked)
        return 0;

    mat[i][j].marked = true;

    return 1 + GetBasinSize(mat, i - 1, j) + GetBasinSize(mat, i + 1, j) + GetBasinSize(mat, i, j - 1) + GetBasinSize(mat, i, j + 1);
}


const part2 = (mat) => {
    mat = mat.map(row => row.map(item => {
        return { "value": parseInt(item), "marked": false };
    }));

    let basins = []

    for (let i = 0; i < mat.length; i++) {
        for (let j = 0; j < mat[0].length; j++) {
            let currentVal = mat[i][j].value;

            let shiftLeft = i - 1;
            let shiftRight = i + 1;
            let shiftUp = j - 1;
            let shiftDown = j + 1;

            if (shiftLeft >= 0 && mat[shiftLeft][j].value <= currentVal)
                continue;

            if (shiftRight < mat[0].length && mat[shiftRight][j].value <= currentVal)
                continue;

            if (shiftUp >= 0 && mat[i][shiftUp].value <= currentVal)
                continue;

            if (shiftDown < mat.length && mat[i][shiftDown].value <= currentVal)
                continue;

            basins.push(GetBasinSize(mat, i, j));
        }
    }

    basins.sort((a, b) => { return b - a; });

    return basins[0] * basins[1] * basins[2];
}

let arr = fs.readFileSync('./input.txt').toString().split('\n').filter(row => row !== '');
let mat = []
arr.forEach(row => mat.push([...row]));

console.log(part1(mat));
console.log(part2(mat));
